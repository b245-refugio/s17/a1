// console.log("Hello World");
/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	
	function userInfo(){
		let name = prompt("What is your name?");
		let age = prompt("How old  are you?");
		let address = prompt("Where do you live");

		console.log("Hello, " + name);
		console.log("You are " + age + " years old.")
		console.log("You live in " + address);
		alert("Thank you for your input!");
	}
	userInfo();
	

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function favBands(){
		let band1 = "Mayonnaise";
		let band2 = "Parokya ni Edgar";
		let band3 = "The Carpenters";
		let band4 = "Maroon 5";
		let band5 = "Orange and Lemons";

		console.log("1. "+band1);
		console.log("2. "+band2);
		console.log("3. "+band3);
		console.log("4. "+band4);
		console.log("5. "+band5);

	}
	favBands();
/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	function favMovies(){
		let movie1 = "Arrow";
		let movie2 = "Avatar: The Way of Water";
		let movie3 = "Black Panter";
		let movie4 = "Men in Black (1997)";
		let movie5 = "Puss in Boots: The Last Wish";



		console.log("1. " + movie1);
		console.log("Rotten Tomatoes Rating: 62%");
		console.log("2. " + movie2);
		console.log("Rotten Tomatoes Rating: 77%");
		console.log("3. " + movie3);
		console.log("Rotten Tomatoes Rating: 96%");
		console.log("4. " + movie4);
		console.log("Rotten Tomatoes Rating: 91%");
		console.log("5. " + movie5);
		console.log("Rotten Tomatoes Rating: 95%");
	}
	favMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();
let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();
// console.log(friend1);
// console.log(friend2);